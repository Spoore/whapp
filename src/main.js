import Vue from 'vue'
import App from './App.vue'
import Button from "ant-design-vue/lib/button";
import "ant-design-vue/dist/antd.css";
import Drawer from 'ant-design-vue/lib/drawer';
import Tabs from 'ant-design-vue/lib/tabs';
import List from 'ant-design-vue/lib/list';
import Avatar from 'ant-design-vue/lib/avatar';
import Input from 'ant-design-vue/lib/input';
import Popover from 'ant-design-vue/lib/popover';
import Icon from 'ant-design-vue/lib/icon';
import Select from 'ant-design-vue/lib/select';
import Divider from 'ant-design-vue/lib/divider';

Vue.component(Button.name, Button);
Vue.component(Drawer.name, Drawer);
Vue.use(Tabs);
Vue.use(List);
Vue.use(Avatar);
Vue.use(Input);
Vue.use(Popover);
Vue.use(Icon);
Vue.use(Select);
Vue.use(Divider);

Vue.config.productionTip = false;


new Vue({
    render: h => h(App),
}).$mount('#app');
